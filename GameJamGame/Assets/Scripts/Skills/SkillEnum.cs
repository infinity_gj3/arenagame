namespace Skills
{
    public enum SkillEnum
    {
        Nothing,
        Heal,
        Punch,
        InvertSpeed,
        Freeze,
        Ice,
        Flash,
        HPTrade,
        Invisible
        
    }
}